package main

import (
	"io"
	"os"

	"bitbucket.org/_metalogic_/colorable"
)

func main() {
	io.Copy(colorable.NewColorableStdout(), os.Stdin)
}
